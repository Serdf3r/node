console.log("general ready!");
$(document).ready(function () {
    // --- Datatable
    $("#tb_elenco_pratiche").dataTable({
        responsive: true,
        "language": {
            "lengthMenu": "Mostra _MENU_ records per pagina",
            "zeroRecords": "Nessun dato trovato - sorry",
            "info": "Mostra pagina _PAGE_ di _PAGES_",
            "infoEmpty": "Nessun Dato",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "sSearch": "Cerca anche per più filtri:",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Prima",
                "sLast": "Ultima",
                "sNext": "Seguente",
                "sPrevious": "Precedente"
            }
        },

        "searching": true,
        "columnDefs": [

            {
                "targets": [0],
            },
            {
                "targets": 9,
                "width": "1%",
                "className": "text-center"
            },
            {
                "targets": 10,
                "width": "1%",
                "orderable": false,
                "className": "text-center"
            }
        ]
    });
    var table = $('#tb_elenco_pratiche').DataTable();
    // --- Ricerca per filtri stato avanzamento e tipo pratica start
    $("#tb_elenco_pratiche_filter.dataTables_filter").append($("#categoryFilter"));
    var categoryIndex_a = 2;
    var categoryIndex_b = 3;
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                let selectedItem = $('#stato_di_avanzamento').val();
                let category = data[categoryIndex_b];
                if (selectedItem === "" || category.includes(selectedItem)) {
                    return true;
                }
                return false;
            }
    );
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var selectedItem_tp = $('#tipo_pratica').val();
                var category = data[categoryIndex_a];
                if (selectedItem_tp === "" || category.includes(selectedItem_tp)) {
                    return true;
                }
                return false;
            }
    );
    $("#stato_di_avanzamento").change(function (e) {
        table.draw();
    });
    $("#tipo_pratica").change(function (e) {
        table.draw();
    });
    // --- Ricerca per filtri stato avanzamento e tipo pratica end
    // --- Ricerca per data start
    var minDate, maxDate;
    minDate = new DateTime($("#min").val(), {
        format: 'YYYY-MM-DD'
    });
    maxDate = new DateTime($("#max").val(), {
        format: 'YYYY-MM-DD'
    });
    var minDate, maxDate;
    $.fn.dataTable.ext.search.push(
            function (settings, data, dataIndex) {
                var min = minDate.val();
                var max = maxDate.val();
                var date = new Date(data[1]);

                if (
                        (min === null && max === null) ||
                        (min === null && date <= max) ||
                        (min <= date && max === null) ||
                        (min <= date && date <= max)
                        ) {
                    return true;
                }
                return false;
            }
    );

    minDate = new DateTime($('#min'), {
        format: 'MMMM Do YYYY'
    });
    maxDate = new DateTime($('#max'), {
        format: 'MMMM Do YYYY'
    });

    $('#min, #max').on('change', function () {
        table.draw();
    });
    // --- Ricerca per data end

    table.draw();


});
