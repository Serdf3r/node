console.log("utils ready!");
$(document).ready(function () {
    $('.gender').click(function () {
        $(this).find('.btn').toggleClass('active');
        if ($(this).find('.btn-primary').length > 0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }
        $(this).find('.btn').toggleClass('btn-default');
    });
    // Reset the form
    $(document).on('click', "#ResetForm", function () {
        document.getElementById('form_nuova_ass').reset();
    });
});
