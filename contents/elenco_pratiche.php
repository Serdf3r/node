<!-- Create the drop down filter -->
<div class="row category-filter p-1">
    <div class="col-12 col-md-3 p-2">
        <select id="tipo_pratica" class="form-select select-sm">
            <option value="">Scegli tipo pratica</option>
            <option value="Assunzione">Assunzione</option>
            <option value="Cessazione">Cessazione</option>
            <option value="Proroga/Trasformazione">Proroga/Trasformazione</option>
            <option value="Variazione orario">Variazione orario</option>
        </select>
    </div>
    <div class="col-12 col-md-3 p-2">
        <select id="stato_di_avanzamento" class="form-select select-sm">
            <option value="">Scegli stato di avanzamento</option>
            <option value="Bozza">Bozza</option>
            <option value="Inviata">Inviata</option>
            <option value="Completata">Completata</option>
        </select>
    </div>


    <div class="col-6 col-md-3 p-2">
        <input type="text" class="form-control"  id="min" name="min" placeholder="Data Inizio">
    </div>
    <div class="col-6 col-md-3 p-2">
        <input type="text" class="form-control"  id="max" name="max" placeholder="Data Fine">
    </div>
</div>
<table id="tb_elenco_pratiche"  class="col-12 col-md-12  p-1 table table-striped display table-bordered"  style="width:100%">
    <thead>
        <tr>
            <th>Pratica</th>
            <th>Data</th>
            <th>Tipo Pratica</th>
            <th>Stato di Avanzamento</th>
            <th>Cognome</th>
            <th>Nome</th>
            <th>Codice Fiscale</th>
            <th>Data di Nascita</th>
            <th>Luogo di Nascita</th>
            <th>Sesso</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
//        include '../config/config.php';

        if ($conn == false) {
            die("ERROR: Could not connect. "
                    . mysqli_connect_error());
        }
        $sql = "SELECT * FROM elenco_pratiche ORDER BY ID DESC";
        if ($res = mysqli_query($conn, $sql)) {
            if (mysqli_num_rows($res) > 0) {
                while ($row = mysqli_fetch_array($res)) {
                    echo "<tr>";
                    echo "<td>" . $row["ID"] . "</td>";
                    echo "<td>" . $row["DATA_PRATICA"] . "</td>";
                    echo "<td>" . $row["TIPO"] . "</td>";
                    echo "<td>" . $row["AVANZAMENTO"] . "</td>";
                    echo "<td>" . $row["COGNOME"] . "</td>";
                    echo "<td>" . $row["NOME"] . "</td>";
                    echo "<td>" . $row["CODICE FISCALE"] . "</td>";
                    echo "<td>" . $row["DATA DI NASCITA"] . "</td>";
                    echo "<td>" . $row["LUOGO NASCITA"] . "</td>";
                    echo "<td>" . $row["SESSO"] . "</td>";
                    switch ($row["AVANZAMENTO"]) {
                        case "Bozza":
                            echo '<td><button type="button" class="btn btn-sm btn-warning">Completa</button></td>';
                            break;
                        case "Completata":
                            echo '<td><button type="button" class="btn btn-sm btn-primary">Invia</button></td>';
                            break;
                        case "Inviata":
                            echo '<td><button type="button" class="btn btn-sm btn-info">Vedi</button></td>';
                            break;
                    }
                    ?>
                <td><button type="button" class="btn btn-sm btn-secondary">Modifica</button></td>
                <td><button type="button" class="btn btn-sm btn-danger">Elimina</button></td>
                <?php
                echo "</tr>";
            }
        }
    }
    ?>
</tbody>
<!--    <tfoot>
    <tr>
        <th>Name</th>
        <th>Position</th>
        <th>Office</th>
        <th>Age</th>
        <th>Start date</th>
        <th>Salary</th>
    </tr>
</tfoot>-->
</table>