<div class="container responsive-tabs p-1">
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a id="tab-A" href="#anagrafica" class="nav-link active" data-bs-toggle="tab" role="tab">Dati Anagrafici</a>
        </li>
        <li class="nav-item">
            <a id="tab-B" href="#extracomunitario" class="nav-link" data-bs-toggle="tab" role="tab">Cittadino Extracomunitario</a>
        </li>
        <li class="nav-item">
            <a id="tab-C" href="#giurisdizione" class="nav-link" data-bs-toggle="tab" role="tab">Giurisdizione</a>
        </li>
        <li class="nav-item">
            <a id="tab-D" href="#assunzione" class="nav-link" data-bs-toggle="tab" role="tab">Assunzione</a>
        </li>
        <li class="nav-item">
            <a id="tab-E" href="#retribuzione" class="nav-link" data-bs-toggle="tab" role="tab">Retribuzione</a>
        </li>
        <li class="nav-item">
            <a id="tab-F" href="#file" class="nav-link" data-bs-toggle="tab" role="tab">File</a>
        </li>
    </ul>
    <form>
        <div id="content" class="tab-content" role="tablist">
            <div id="anagrafica" class="card tab-pane fade show active border border-top-0  mb-1" role="tabpanel" aria-labelledby="tab-A">
                <div class="card-header" role="tab" id="heading-A">
                    <h5 class="mb-0">
                        <a data-bs-toggle="collapse" href="#collapse-A" aria-expanded="true" aria-controls="collapse-A">
                            Dati Anagrafici
                        </a>
                    </h5>
                </div>
                <div id="collapse-A" class="collapse show " data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-A">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-3  p-1 mt-0">
                                <label for="ana_cognome" class="form-label">COGNOME</label>
                                <input type="text" class="form-control" name="ana_cognome"  id="cognome" required>
                                <div class="invalid-feedback"> </div>
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ana_nome" class="form-label">NOME</label>
                                <input type="text" class="form-control" name="ana_nome" id="nome" required>
                            </div>
                            <div class="col-12 col-md-4 p-1 mt-0">
                                <label for="ana_cod_fiscale" class="form-label">CODICE FISCALE</label>
                                <input type="text" class="form-control" maxlength="16"  pattern=".{16,16}"   required title="Cofice Fiscale valido richiesto" id="ana_cod_fiscale" name="cod_fiscale" placeholder="">
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ana_sesso" class="form-label">SESSO</label>
                                <div class="btn-group btn-toggle gender form-control p-0 m-0"> 
                                    <input type="button" name="ana_sesso" class="btn btn-default" value="Maschio">
                                    <input type="button" name="ana_sesso" class="btn btn-primary active" value="Femmina">
                                </div>
                            </div>

                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ana_data_nascita" class="form-label">DATA DI NASCITA </label>
                                <input type="date" class="form-control" id="ana_data_nascita">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ana_luogo_nascita" class="form-label">LUOGO NASCITA</label>
                                <input type="text" class="form-control" id="ana_luogo_nascita">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ana_stato_civile" class="form-label">STATO CIVILE</label>
                                <select class="form-select" id="ana_stato_civile" aria-label="Default select example">
                                    <option selected>Scegli</option>
                                    <option value="1">Celibe/Nubile</option>
                                    <option value="2">Coniugato/a</option>
                                    <option value="3">Vedovo/a</option>
                                    <option value="3">Divorziato/a</option>                        
                                </select>   
                            </div>
                            <div class="col-12 col-md-4 p-1 mt-0">
                                <label for="ana_cittadinanza" class="form-label">CITTADINANZA</label>
                                <input type="text" class="form-control" id="ana_cittadinanza">
                            </div>

                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="ana_residenza" class="form-label">RESIDENZA</label>
                                <input type="text" class="form-control" id="ana_residenza">
                            </div>
                            <div class="col-12 col-md-4 p-1 mt-0">
                                <label for="ana_citta" class="form-label">CITTA'</label>
                                <input type="text" class="form-control" id="ana_citta">
                            </div>
                            <div class="col-6 col-md-1 p-1 mt-0">
                                <label for="ana_cap" class="form-label">CAP</label>
                                <input type="text" class="form-control" maxlength="5"  pattern=".{5,5}"   required title="Cap valido richiesto" id="ana_cap">
                            </div>
                            <div class="col-6 col-md-1 p-1 mt-0">
                                <label for="ana_prov" class="form-label">PROVINCIA</label>
                                <input type="text" class="form-control" maxlength="2"  pattern=".{2,2}"   required title="Sigla Provincia valida richiesta" id="ana_prov">
                            </div>

                            <div class="col-12 col-md-4 p-1 mt-0">
                                <label for="ana_cod_fiscale_con" class="form-label">CODICE FISCALE DEL CONIUGE anche se non a carico</label>
                                <input type="text" class="form-control" maxlength="16"  pattern=".{16,16}"  title="Cofice Fiscale valido richiesto" id="ana_cod_fiscale_con">
                            </div>
                            <div class="col-12 col-md-8 p-1 mt-0">
                                <label for="ana_studio" class="form-label">TITOLO DI STUDIO</label>
                                <input type="text" class="form-control" id="ana_studio">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="extracomunitario" class="card tab-pane fade border border-top-0  mb-1" role="tabpanel" aria-labelledby="tab-B">
                <div class="card-header" role="tab" id="heading-B">
                    <h5 class="mb-0">
                        <a class="collapsed" data-bs-toggle="collapse" href="#collapse-B" aria-expanded="false"
                           aria-controls="collapse-B">
                            Cittadino Extracomunitario
                        </a>
                    </h5>
                </div>
                <div id="collapse-B" class="collapse " data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-B">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="ext_titolo_sog" class="form-label">TITOLO SOGGIORNO</label>
                                <input type="text" class="form-control" id="ext_titolo_sog">
                            </div>
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="ext_titolo_num" class="form-label">NUMERO TITOLO</label>
                                <input type="text" class="form-control" id="ext_titolo_num">
                            </div>
                            <div class="col-12 col-md-12 p-1 mt-0">
                                <label for="ext_questura" class="form-label">QUESTURA DI RILASCIO</label>
                                <input type="text" class="form-control" id="ext_questura">
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ext_data_ril" class="form-label">DATA RILASCIO</label>
                                <input type="date" class="form-control" id="ext_data_ril">
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ext_data_sca" class="form-label">DATA SCADENZA</label>
                                <input type="date" class="form-control" id="ext_data_sca">
                            </div>
                            <div class="col-12 col-md-8 p-1 mt-0">
                                <label for="ext_motivo" class="form-label">MOTIVO PERMESSO</label>
                                <input type="text" class="form-control" id="ext_motivo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="giurisdizione" class="card tab-pane fade border border-top-0  mb-1" role="tabpanel" aria-labelledby="tab-C">
                <div class="card-header" role="tab" id="heading-C">
                    <h5 class="mb-0">
                        <a data-bs-toggle="collapse" href="#collapse-C" aria-expanded="true" aria-controls="collapse-C">
                            Giurisdizione
                        </a>
                    </h5>
                </div>
                <div id="collapse-C" class="collapse " data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-C">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="giu_part_time" class="form-label">ALTRO LAVORO PART TIME</label>
                                <div class="btn-group btn-toggle gender form-control p-0 m-0"> 
                                    <input type="button" name="giu_part_time" class="btn btn-default" value="SI">
                                    <input type="button" name="giu_part_time" class="btn btn-primary active" value="NO">
                                </div>
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="giu_unione_civ" class="form-label">UNIONE CIVILE</label>
                                <div class="btn-group btn-toggle gender form-control p-0 m-0"> 
                                    <input type="button" name="giu_unione_civ" class="btn btn-default" value="SI">
                                    <input type="button" name="giu_unione_civ" class="btn btn-primary active" value="NO">
                                </div>
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="giu_minori" class="form-label">MINORI</label>
                                <div class="btn-group btn-toggle gender form-control p-0 m-0"> 
                                    <input type="button" name="giu_minori" class="btn btn-default" value="SI">
                                    <input type="button" name="giu_minori" class="btn btn-primary active" value="NO">
                                </div>
                            </div>
                            <div class="col-12 col-md-12 p-1 mt-0">
                                <label for="giu_casell_giud" class="form-label">CERTIFICATO CASELLARIO GIUDIZIALE</label>
                                <input type="text" class="form-control" id="giu_casell_giud" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="assunzione" class="card tab-pane fade border border-top-0  mb-1" role="tabpanel" aria-labelledby="tab-D">
                <div class="card-header" role="tab" id="heading-D">
                    <h5 class="mb-0">
                        <a data-bs-toggle="collapse" href="#collapse-D" aria-expanded="true" aria-controls="collapse-D">
                            Assunzione
                        </a>
                    </h5>
                </div>
                <div id="collapse-D" class="collapse" data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-D">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ass_data_start" class="form-label">DATA INIZIO</label>
                                <input type="date" class="form-control" id="ass_data_start">
                            </div>
                            <div class="col-12 col-md-2 p-1 mt-0">
                                <label for="ass_data_end" class="form-label">DATA FINE</label>
                                <input type="date" class="form-control" id="ass_data_end">
                            </div>
                            <div class="col-12 col-md-8 p-1 mt-0">
                                <label for="ass_agevolazioni" class="form-label">AGEVOLAZIONI</label>
                                <input type="text" class="form-control" id="ass_agevolazioni" placeholder="">
                            </div>
                            <div class="col-12 col-md-4 p-1 mt-0">
                                <label for="ass_tipo_rapp" class="form-label">TIPO RAPPORTO</label>
                                <select class="form-select" id="ass_tipo_rapp" aria-label="Default select example">
                                    <option selected>Scegli</option>
                                    <option value="1">Tempo determinato per sostituzione</option>
                                    <option value="2">Tempo determinato acausale</option>
                                    <option value="3">Tempo determinato stagionale</option>
                                    <option value="4">Tempo indeterminato</option>                   
                                    <option value="5">Intermittente</option>    
                                    <option value="6">Apprendistato</option>    
                                </select>  
                            </div>
                            <div class="col-12 col-md-8 p-1 mt-0">
                                <label for="ass_pers_sost" class="form-label">PERSONA SOSTITUITA</label>
                                <input type="text" class="form-control" id="ass_pers_sost" placeholder="">
                            </div>

                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ass_mansione" class="form-label">MANSIONE</label>
                                <input type="text" class="form-control" id="ass_mansione">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ass_livello" class="form-label">LIVELLO</label>
                                <input type="text" class="form-control" id="ass_livello">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ass_qualifica" class="form-label">QUALIFICA</label>
                                <input type="text" class="form-control" id="ass_qualifica" placeholder="">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ass_monte_ore" class="form-label">MONTE ORE</label>
                                <input type="number" class="form-control" id="ass_monte_ore" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="retribuzione" class="card tab-pane fade border border-top-0  mb-1" role="tabpanel" aria-labelledby="tab-E">
                <div class="card-header" role="tab" id="heading-E">
                    <h5 class="mb-0">
                        <a data-bs-toggle="collapse" href="#collapse-E" aria-expanded="true" aria-controls="collapse-E">
                            Retribuzione
                        </a>
                    </h5>
                </div>
                <div id="collapse-E" class="collapse" data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-E">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_inail" class="form-label">PAT INAIL</label>
                                <input type="text" class="form-control" id="ret_inail">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_tariffa" class="form-label">VOCE TARIFFA</label>
                                <input type="text" class="form-control" id="ret_tariffa">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_inps" class="form-label">POSIZIONE INPS</label>
                                <input type="text" class="form-control" id="ret_inps" placeholder="">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_sede" class="form-label">SEDE DI LAVORO</label>
                                <input type="text" class="form-control" id="ret_sede" placeholder="">
                            </div>

                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_citta" class="form-label">CITTA</label>
                                <input type="text" class="form-control" id="ret_citta">
                            </div>
                            <div class="col-12 col-md-5 p-1 mt-0">
                                <label for="ret_via" class="form-label">VIA </label>
                                <input type="text" class="form-control" id="ret_via">
                            </div>
                            <div class="col-12 col-md-1 p-1 mt-0">
                                <label for="ret_prov" class="form-label">PROVINCIA</label>
                                <input type="text" class="form-control" maxlenght="2" pattern=".{2,2}"   required title="Sigla Provincia valida richiesta"  id="ret_prov" placeholder="">
                            </div>
                            <div class="col-12 col-md-3 p-1 mt-0">
                                <label for="ret_pagamento" class="form-label">PAGAMENTO</label>
                                <select class="form-select" id="ret_pagamento" aria-label="Default select example">
                                    <option selected>Scegli</option>
                                    <option value="1">Iban</option>
                                    <option value="2">Contante</option>
                                    <option value="3">Assegno Circolare</option>
                                    <option value="3">Assegno Bancario</option>                        
                                </select>                    
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div id="file" class="card tab-pane fade border border-top-0  mb-1 " role="tabpanel" aria-labelledby="tab-F">
                <div class="card-header" role="tab" id="heading-F">
                    <h5 class="mb-0">
                        <a data-bs-toggle="collapse" href="#collapse-F" aria-expanded="true" aria-controls="collapse-F">
                            Caricamento File
                        </a>
                    </h5>
                </div>
                <div id="collapse-F" class="collapse" data-bs-parent="#content" role="tabpanel"
                     aria-labelledby="heading-F">
                    <div class="card-body">
                        <div class="row g-3">
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="car_ide" class="form-label">Carta d'Identità</label>
                                <input class="form-control" type="file" id="car_ide">
                            </div>
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="car_cod" class="form-label">Codice Fiscale</label>
                                <input class="form-control" type="file" id="car_cod">
                            </div>
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="car_per" class="form-label">Permesso di Soggiorno</label>
                                <input class="form-control" type="file" id="car_per">
                            </div>
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="car_sva" class="form-label">Certificato di svantaggio (L 381/91, 68/99)</label>
                                <input class="form-control" type="file" id="car_sva">
                            </div>
                            <div class="col-12 col-md-6 p-1 mt-0">
                                <label for="car_cas" class="form-label">Certificato casellario giudiziale</label>
                                <input class="form-control" type="file" id="car_cas">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-12 pt-1">
                <button type="submit" id="GoForm" class="btn btn-primary">Conferma</button>
                <button type="button" id="ResetForm" class="btn btn-secondary">Reset</button>
            </div>
        </div>
    </form>
</div>
