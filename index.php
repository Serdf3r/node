<?php
include 'config/config.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!--Bootstrap CSS--> 
        <link rel="stylesheet" href="assets/bootstrap-5.1.3-dist/css/bootstrap.min.css" crossorigin="anonymous">
        <!--General CSS--> 
        <link rel="stylesheet" href="css/general.css">

        <!--Jquery JS--> 
        <script async src="assets/js/jquery-3.6.0.min.js" ></script>
        <script async src="assets/js/jquery-ui.js" ></script>
        <!--JQuery CDN--> 
        <script type="text/javascript" charset="utf8" src=" https://code.jquery.com/jquery-3.6.0.js"></script>

        <!--Bootstrap JS--> 
        <script async src="assets/bootstrap-5.1.3-dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
        <!--General/Utils JS--> 
        <script async src="js/general.js" crossorigin="anonymous"></script>
        <script async src="js/utils.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.11.5/b-2.2.2/b-html5-2.2.2/date-1.1.2/r-2.2.9/datatables.min.css"/>
        <script async src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
      <script async src=" https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js" crossorigin="anonymous"></script>

       
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.11.5/b-2.2.2/b-html5-2.2.2/date-1.1.2/r-2.2.9/datatables.min.js"></script>
    </head>
    <body>
        <div class="container-fluid container">
            <?php
            include 'layout/header.php';
            ?>
            
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#elenco_pratiche" type="button" role="tab" aria-controls="elenc" aria-selected="true">Elenco Pratiche</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#nuova_assunzione" type="button" role="tab" aria-controls="profile" aria-selected="false">Nuova Assunzione</button>
                </li>                
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active border border-top-0 p-1" id="elenco_pratiche" role="tabpanel" aria-labelledby="home-tab"> <?php
                    include 'contents/elenco_pratiche.php';
                    ?></div>
                <div class="tab-pane fade border border-top-0 p-1" id="nuova_assunzione" role="tabpanel" aria-labelledby="profile-tab"> <?php
                    include 'contents/assunzione.php';
                    ?></div>
            </div>
            <?php
            include 'layout/footer.php';
            ?>
        </div>
    </body>